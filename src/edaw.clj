(ns edaw
  (:require [clojure.core.async :refer [go <! timeout]]
            [clojure.reflect :refer [reflect]]
            [gstreamer.graph.low-level :as gll]
            [gstreamer.pipeline :as p]
            [gstreamer.pipeline.diff :refer [diff]])
  (:import [java.util.concurrent TimeUnit]
           [java.lang Runnable]
           [org.freedesktop.gstreamer Gst Pipeline Version ElementFactory Bin State]))

(defn ensure-root-bin! [graph]
  (vary-meta graph update-in [::gst ::root] #(or % (Bin. "main"))))

(defn create! [graph named {:keys [element]}]
  (when-let [el (some-> element
                        name
                        ElementFactory/find
                        (.create (name named)))]
    (-> graph meta ::gst ::root (.add el))
    (vary-meta graph assoc-in [::gst named] el)))

(defn link! [graph k s]
  (let [el (-> graph meta ::gst k)
        link! #(.link (-> graph meta ::gst %)
                      el)]
    (run! link! s))
  graph)

(defn set-state! [graph state]
  (let [new-state ({:playing State/PLAYING
                    :paused State/PAUSED} state)]
    (.setState (::root (::gst (meta graph))) new-state)
    graph))

(defn state [graph]
  (-> graph meta ::gst ::root .getState))

(defn patched? [graph new-graph]
  (let [{:keys [elements connections props on]}
        (diff graph new-graph)
        touched? (some-fn (comp not empty? :add)
                          (comp not empty? :remove))]
    (not-any? touched? [elements])))

(defn patch! [graph new-graph]
  (let [{:keys [elements connections]} (diff graph new-graph)]
    (-> new-graph
        ensure-root-bin!
        ((partial reduce-kv create!) (-> elements :add vals))
        ((partial reduce-kv link!) (:add connections)))))

(comment
  (let [graph-atom (-> (gll/empty-graph!)
                     (gll/create-element! :audiotestsrc :SRC)
                     (gll/create-element! :autoaudiosink :SNK)
                     (gll/connect-elements! :SRC :SNK))]
    (go
      (gll/set-state! graph-atom :playing)
      (-> @graph-atom :root-bin .getState println)
      (<! (timeout 2000))
      (gll/set-state! graph-atom :paused)
      (-> @graph-atom :root-bin .getState println)))

  (defn test-graph! [] (-> {}
                           ensure-root-bin!
                           (create! :audiotestsrc (p/el :audiotestsrc {:named :SRC}))
                           (create! :autoaudiosink (p/el :autoaudiosink {:named :SNK}))
                           (link! :SNK #{:SRC})))

  (let [graph (test-graph!)]
    (go (set-state! graph :playing)
        (<! (timeout 2000))
        (set-state! graph :paused)))

  (def graph-atom (atom (test-graph!)))
  (add-watch graph-atom :patch
             (fn [k a old new]
               (when-not (doto (patched? old new) println)
                 (println "patching!")
                 (println (diff old new))
                 (reset! a (patch! old new)))))

    (doto graph-atom
      (swap! merge (p/graph
                    (p/pipe
                     (p/el :videotestsrc {:named :SRC})
                     (p/el :autovideosink {:named :SNK})))))
  nil)
