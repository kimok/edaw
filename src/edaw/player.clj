(ns edaw.player
  (:require [edaw.transport :as transport]
            [clojure.core.async :refer [go-loop <! timeout put!]]
            [edaw.test :as test]
            [edaw.app :as app]
            [membrane.java2d :as java2d]
            [membrane.component :as mc]
            [membrane.ui :as ui]
            [edaw.fader :as fader :refer [fader]]
            [edaw.graph :as graph]
            [edaw.mixer :refer [mixer]]
            [edaw.transport :as transport :refer [transport reel]]
            [edaw.toggle :as toggle]
            [gstreamer.graph.low-level :as gll]))

(mc/defui player [{:keys [graph playing? position state channels app close-ch] :as m}]
  [(app/quit {:graph graph :close-ch close-ch :app app})
   (ui/on :key-press #(case %1
                       "p" [[::transport/play-pause
                             {:graph graph :on? playing?}]
                            [:update $playing? not]]
                       :page_up [[::transport/seek
                                  {:graph graph :position (+ position 0.05)}]]
                       :page_down [[::transport/seek
                                    {:graph graph :position (- position 0.05)}]]
                       nil)
         (ui/vertical-layout
          (reel
           {:graph graph :playing? playing? :position position :state state})
          (mixer
           {:graph graph :playing? playing? :channels channels})))])
