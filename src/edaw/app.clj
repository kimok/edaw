(ns edaw.app
  (:require [edaw.transport :as transport]
            [clojure.core.async :refer [put!]]
            [membrane.component :as mc]
            [membrane.ui :as ui]))

(mc/defeffect ::quit [{:keys [graph app close-ch] :as m}]
  (when close-ch (put! close-ch true))
  (when graph (transport/pause! graph))
  (when app (.dispose (:membrane.java2d/frame app))))

(mc/defui quit [m]
  (ui/on :key-press #(when (= "q" %) [[::quit m]]) []))
