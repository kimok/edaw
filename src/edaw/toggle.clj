(ns edaw.toggle
  (:require [membrane.component :as mc]
            [membrane.java2d :as java2d]
            [membrane.ui :as ui]
            [edaw.ui]))

(mc/defui triangle [{:keys [w h]
                     :or {w 50 h 70}}]
  (ui/path [0 0] [w (/ h 2)] [0 h]))

(mc/defui pause-bars [{:keys [w h]}]
  (let [bar (ui/rectangle (/ w 3) h)]
    [bar (->> bar (ui/translate (* w 2/3) 0))]))

(mc/defeffect ::pressed [{:keys [$on?]}]
  (dispatch! :update $on? not))

(mc/defui toggle [{:keys [w h on? colors]
                   :or {w 100 h 100 colors [:gray :black]}}]
  (ui/on :mouse-down (fn [_] [[::pressed {:$on? $on? :on? on?}]])
         (edaw.ui/color ((if on? second first) colors)
                        (ui/rectangle w h))))

(mc/defui play [{:keys [w h on? colors]
                 :or {w 100 h 100 colors [:green :yellow]}}]
  (let [bg (toggle {:w w :h h :on? on? :colors colors})]
    [bg
     (-> ((if on? pause-bars triangle)
          {:w (* w 1/2) :h (* h 2/3)})
         (ui/center (ui/bounds bg)))]))

(mc/defui mute [{:keys [w h on? colors]
                 :or {colors [:green :red]}}]
  (toggle {:w w :h h :on? on? :colors colors}))

(mc/defui solo [{:keys [w h on? colors]
                 :or {colors [:orange :blue]}}]
  (toggle {:w w :h h :on? on? :colors colors}))

#_(java2d/run (mc/make-app #'toggle {}))
#_(java2d/run (mc/make-app #'play {}))
