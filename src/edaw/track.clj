(ns edaw.track
  (:require [clojure.core.async :refer [go <! timeout]]
            [edaw.test :as test]
            [edaw.graph :as g]
            [gstreamer.graph.low-level :as gll]))


(defn add-channel! [graph i src-pad sink]
  (let [types [:volume :audiopanorama]
        ->name #(keyword (str (name %) "-" (str i)))
        names (map ->name types)]
    (doto graph
      (g/element! (zipmap types names))
      (g/sync! names)
      (g/link! (zipmap (butlast names)
                       (rest names))))
    (let [sink-pad (-> @graph
                       (get (first names))
                       :reference
                       .getSinkPads
                       first)]
      (.link src-pad sink-pad)
      (g/link! graph {(last names) sink}))))

(comment
  (let [graph (-> (test/deinterleaved-src!)
                  (g/element! {:autoaudiosink :SINK}))
        deinterleave (-> @graph :DEINTERLEAVE :reference)]
    (g/on deinterleave :pad-added
          (fn [el src-pad]
            (when (#{"src_0"} (.getName src-pad))
              (add-channel! graph 0 src-pad :SINK))))
    (go
      (gll/set-state! graph :playing)
      (<! (timeout 5000))
      (gll/set-state! graph :paused)))
  nil)
