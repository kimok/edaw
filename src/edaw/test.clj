(ns edaw.test
  (:require [gstreamer.graph.low-level :as gll])
  (:import [org.freedesktop.gstreamer Pipeline]))

(defn init! []
  (atom {:root-bin (Pipeline. "main")}))

(defn tone []
  (-> (init!)
      (gll/create-element! :audiotestsrc :SRC)
      (gll/create-element! :autoaudiosink :SNK)
      (gll/connect-elements! :SRC :SNK)))

(defn wav-src! []
  (-> (init!)
      (gll/create-element! :filesrc :SRC)
      (gll/set-prop! :SRC :location "demo.wav")
      ((partial reduce-kv gll/create-element!)
       {:wavparse :PARSE
        :audioconvert :CONVERT
        :autoaudiosink :SINK})
      ((partial reduce-kv gll/connect-elements!)
       {:SRC :PARSE
        :PARSE :CONVERT
        :CONVERT :SINK})))

(defn deinterleaved-src! []
  (-> (init!)
      (gll/create-element! :filesrc :SRC)
      (gll/set-prop! :SRC :location "demo.wav")
      ((partial reduce-kv gll/create-element!)
       {:wavparse :PARSE
        :audioconvert :CONVERT
        :deinterleave :DEINTERLEAVE})
      ((partial reduce-kv gll/connect-elements!)
       {:SRC :PARSE
        :PARSE :CONVERT
        :CONVERT :DEINTERLEAVE})))

(defn wav->app! []
  (-> (init!)
      (gll/create-element! :filesrc :SRC)
      (gll/set-prop! :SRC :location "demo.wav")
      ((partial reduce-kv gll/create-element!)
       {:wavparse :PARSE
        :audioconvert :CONVERT
        :appsink :APPSINK})
      ((partial reduce-kv gll/connect-elements!)
       {:SRC :PARSE
        :PARSE :CONVERT
        :CONVERT :APPSINK})))
