(ns edaw.launch
  (:require [clojure.core.async :as async :refer [go-loop <! timeout chan alt! put!]]
            [membrane.component :as mc]
            [membrane.java2d :as java2d]
            [edaw.test :as test]
            [edaw.element :as element]
            [edaw.ui]
            [edaw.track :as track]
            [edaw.message :as message]
            [edaw.player :refer [player]]
            [edaw.transport :as transport]
            [edaw.graph :as g]
            [gstreamer.graph.low-level :as gll]))

(defn deinterleaved-src! [filename]
  (-> (test/init!)
      (gll/create-element! :filesrc :SRC)
      (gll/set-prop! :SRC :location filename)
      ((partial reduce-kv gll/create-element!)
       {:wavparse :PARSE
        :audioconvert :CONVERT
        :deinterleave :DEINTERLEAVE})
      ((partial reduce-kv gll/connect-elements!)
       {:SRC :PARSE
        :PARSE :CONVERT
        :CONVERT :DEINTERLEAVE})
      (g/element! {:audiomixer :MIX
                   :autoaudiosink :SINK})
      (g/link! {:MIX :SINK})))

(defn player! [filename]
  (let [graph (deinterleaved-src! filename)
        db (atom {:position 0.8
                  :state :paused
                  :playing? false
                  :close-ch (chan)
                  :graph graph
                  :channels (mapv #(do {:w 150 :h 300 :id % :muted? false})
                                  (range 8))})
        update-position! (fn [db]
                           (let [{:keys [graph app]} @db
                                 repaint (:membrane.java2d/repaint app)
                                 position
                                 (-> @graph
                                     :root-bin
                                     (.queryPosition
                                      org.freedesktop.gstreamer.Format/PERCENT)
                                     (/ 1000000))]
                             (swap! db assoc :position position)
                             (repaint)))]
    (-> @graph :DEINTERLEAVE
        (element/on :pad-added
                    (fn [{:keys [pad]}]
                      (let [id (->> pad
                                    .getName
                                    (re-matches #".*_([0-9]+)")
                                    second)]
                        (track/add-channel! graph id pad :MIX)))))
    (message/on graph :message message/log)
    (swap! db assoc :app (membrane.java2d/run
                          (mc/make-app #'player db)))
    (go-loop []
      (let [{:keys [playing? close-ch]} @db]
        (when playing?
          (update-position! db))
        (alt!
          (timeout 16) (recur)
          close-ch nil)))))
