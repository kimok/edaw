(ns edaw.ui
  (:require [clojure.core.async :as async]
            [membrane.component :as mc]
            [membrane.java2d :as java2d]
            [membrane.lanterna :as lanterna]
            [membrane.ui :as ui]
            [edaw.test :as test]
            [gstreamer.graph.low-level :as gll])
  (:import [membrane.ui EventHandler]))

(def colors {:black [0 0 0]
             :gray [1/4 1/4 1/4]
             :blue [1/4 1/4 1]
             :white [1 1 1]
             :red [1 1/4 1/4]
             :yellow [1 1 1/4]
             :orange [1 3/4 1/8]
             :green [1/4 1 1/4]})

(defmulti color (fn [k & _] k))

(defmethod color :default [_ & drawables]
  (apply color :black drawables))

(doall
 (for [val (keys colors)]
   (defmethod color val [k & drawables]
     (apply ui/with-color (k colors) drawables))))


(defn before [event-type handler drawable]
  (EventHandler. event-type
                 (fn [& args]
                   (into (vector (apply vector event-type args))
                         (apply handler args)))
                 drawable))

(defn after [event-type handler drawable]
  (EventHandler. event-type
                 (fn [& args]
                   (into (apply handler args)
                         (vector (apply vector event-type args))))
                 drawable))
