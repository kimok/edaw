(ns edaw.annotate
  (:require [membrane.ui :as ui]
            [membrane.component :as mc]
            [edaw.transport :refer [reel transport]]
            [edaw.toggle :refer [toggle]]
            [edaw.ui]
            [edaw.app :as app]
            [edaw.test :as test]
            [clojure.core.async
             :refer [<! >! <!! timeout chan alt! go go-loop close! put!]]))

(mc/defeffect ::change-brush [{:keys [key-code
                                      prev-key-code
                                      position
                                      playing?
                                      $annotations
                                      annotations]}]
  (when playing?
    (when (not= key-code prev-key-code)
      (dispatch! :update $annotations conj {:from prev-key-code
                                            :to key-code
                                            :position (float position)}))))

(mc/defui key-picker [{:keys [test-ann key-code graph colors close-ch app waveform
                              position playing? w h annotations] :as m}]
  [(app/quit {:graph graph :close-ch close-ch :app app})
   (ui/vertical-layout
    (into [(transport {:w w :h h
                       :graph graph :position position
                       :waveform waveform
                       :playing? playing?})]
          (for [[beg end] (partition 2 1 annotations)]
            (ui/with-color (or (get colors (:to beg)) [0 0 0])
              (ui/translate (* w (:position beg)) 0
                            (ui/rectangle (* w (- (:position end) (:position beg)))
                                          (/ h 8))))))
    (ui/label (float position))
    (ui/label annotations)
    (ui/on :key-press (fn [v] [[::change-brush {:playing? playing?
                                                :prev-key-code key-code
                                                :key-code v
                                                :position position
                                                :$annotations $annotations
                                                :annotations annotations}]
                               [:set $key-code v]])
           (ui/label key-code (ui/font nil 150))))])

#_(do
    (require '[clojure.core.async :refer [go-loop <! timeout]]
             '[edaw.test :as test]
             '[membrane.java2d :as java2d])
    (def db (atom {:position 0.8
                   :close-ch (chan)
                   :playing? false
                   :colors {"a" [0 0 1] "b" [1 0 0]
                            "c" [0 1 0] "d" [1 0 1]}
                   :w 800 :h 300
                   :annotations []
                   :graph (test/wav-src!)}))

    (def app (membrane.java2d/run
              (mc/make-app #'key-picker db)))

    (swap! db assoc :app app)

    (defn update-position! [db]
      (let [{:keys [graph]
             {:membrane.java2d/keys [repaint]} :app} @db
            position
            (-> @graph
                :root-bin
                (.queryPosition org.freedesktop.gstreamer.Format/PERCENT)
                (/ 1000000))]
        (swap! db assoc :position position)
        (repaint)))

    (go-loop []
      (let [{:keys [playing? close-ch]} @db]
        (when playing?
          (update-position! db))
        (alt!
          (timeout 16) (recur)
          close-ch nil))))
