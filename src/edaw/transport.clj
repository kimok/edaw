(ns edaw.transport
  (:require [membrane.component :as mc]
            [membrane.ui :as ui]
            [edaw.fader :as fader :refer [fader]]
            [edaw.graph :as graph]
            [edaw.util :as u]
            edaw.ui
            [edaw.toggle :as toggle]
            [gstreamer.graph.low-level :as gll]))

(defn pause! [graph] (gll/set-state! graph :paused))

(mc/defeffect ::play-pause [{:keys [graph on?] :as m}]
  (gll/set-state! graph (if on? :paused :playing)))

(mc/defeffect ::stopped [{:keys [graph]}]
  (doto graph
    (gll/set-state! :paused)
    (graph/seek! 0)))

(mc/defeffect ::seek [{:keys [position graph]}]
  (graph/seek! graph (* (graph/duration graph) position)))

(mc/defui stop [{:keys [w h]
                 :or {w 100 h 100}}]
  (let [bg (edaw.ui/color :red (ui/rectangle w h))]
    (ui/on :mouse-down (fn [_] [[::stopped {}]])
           [bg
            (ui/center
             (ui/rectangle 50 50)
             (ui/bounds bg))])))

(ui/mouse-down (stop {}) [0 0])

(mc/defui reel [{:keys [w h position graph waveform]
                 :or {position 0 w 200 h 100}}]
  (let [seek (fn [{:keys [level]}] [[::seek {:position level
                                             :graph graph}]])]
    [(ui/on ::fader/moved seek
            ::fader/pressed seek
            (fader {:level position :axis :x :w w :h h}))
     (when waveform
       (->> (for [x (range w)
                  :let [i (int (Math/floor (* (count waveform) (/ x w))))]]
              [x (u/scale (or (get waveform i) 0)
                          -1 1 0 h)])
            (apply ui/path)
            (ui/with-color [1 0 0])
            (ui/with-stroke-width 1)
            (ui/with-style :membrane.ui/style-stroke)))]))

(mc/defui transport [{:keys [position graph playing? waveform w h]
                      :as m}]
  (ui/vertical-layout
   (reel {:position position
          :graph graph
          :waveform waveform
          :w w :h h})
   (ui/horizontal-layout
    (edaw.ui/after ::toggle/pressed
                   (fn [m] [[::play-pause (assoc m :graph graph)]])
                   (toggle/play {:on? playing?}))
    (ui/on ::stopped (fn [_] [[::stopped {:graph graph}]
                              [:set $position 0]
                              [:set $playing? false]])
           (stop {})))))

#_(do
    (require '[clojure.core.async :refer [go-loop <! timeout]]
             '[edaw.test :as test]
             '[membrane.java2d :as java2d])
    (def db (atom {:position 0.8
                   :state :paused
                   :playing? false
                   :graph (test/wav-src!)}))

    (def window (membrane.java2d/run
                 (mc/make-app #'transport db)))

    (swap! db assoc :repaint (:membrane.java2d/repaint window))

    (defn update-position! [db]
      (let [{:keys [graph repaint]} @db
            position
            (-> @graph
                :root-bin
                (.queryPosition org.freedesktop.gstreamer.Format/PERCENT)
                (/ 1000000))]
        (swap! db assoc :position position)
        (repaint)))

    (go-loop []
      (when (:playing? @db)
        (update-position! db)
        (<! (timeout 16)))
      (recur)))
