(ns edaw.app-sink
  (:require [edaw.annotate :refer [key-picker]]
            [membrane.ui :as ui]
            [membrane.component :as mc]
            [edaw.transport :refer [reel transport]]
            [edaw.toggle :refer [toggle]]
            [edaw.message :as message]
            [edaw.ui]
            [edaw.app :as app]
            [edaw.test :as test]
            [gstreamer.graph.low-level :as gll]
            [clojure.core.async
             :refer [<! >! <!! timeout chan alt! go go-loop close! put!]])
  (:import [org.freedesktop.gstreamer FlowReturn]))

(do
  (require '[clojure.core.async :refer [go-loop <! timeout]]
           '[edaw.test :as test]
           '[membrane.java2d :as java2d])
  (def db (atom {:position 0.8
                 :close-ch (chan)
                 :playing? false
                 :colors {"a" [0 0 1] "b" [1 0 0]
                          "c" [0 1 0] "d" [1 0 1]}
                 :w 1920 :h 600
                 :annotations []
                 :waveform-graph (test/wav->app!)
                 :waveform []
                 :graph (test/wav-src!)}))

  (doto (-> @db :waveform-graph deref :APPSINK :reference)
    (.set "emit-signals" true)
    (.set "sync" false)
    (message/on :new-sample
                (fn [{:keys [element]}]
                  (let [bytes (-> element .pullSample
                                  .getBuffer (.map false))]
                    (swap! db update :waveform
                           conj (.getFloat bytes 8))
                    FlowReturn/OK))))

  (-> (@db :waveform-graph) (gll/set-state! :playing))

  (def app (membrane.java2d/run
            (mc/make-app #'key-picker db)))

  (swap! db assoc :app app)

  (defn update-position! [db]
    (let [{:keys [graph]
           {:membrane.java2d/keys [repaint]} :app} @db
          position
          (-> @graph
              :root-bin
              (.queryPosition org.freedesktop.gstreamer.Format/PERCENT)
              (/ 1000000))]
      (swap! db assoc :position position)
      (repaint)))

  (go-loop []
    (let [{:keys [playing? close-ch]} @db]
      (when playing?
        (update-position! db))
      (alt!
        (timeout 16) (recur)
        close-ch nil))))
