(ns edaw.message
  (:require [clojure.string :as str]
            [edaw.graph :as graph]
            [edaw.test :as test])
  (:import [org.freedesktop.gstreamer Bus$MESSAGE Bus$ERROR]))

(defn ->map [message]
  (let [s (some-> message .getStructure)
        n (some-> s .getFields)
        ks (map #(.getName s %)
                (range n))]
    (zipmap ks
            (map (fn [k] (.getValue s k)) ks))))

(defn <-str [& messages]
  (for [message messages]
    (let [s (some-> message .getStructure .toString)
          parse #(apply hash-map
                        (-> %
                            (str/replace #",|;" "")
                            (str/replace #"\(.*?\)" "")
                            (str/replace #"\(" "")
                            (str/replace #"\)" "/")
                            (str/split #"=| ")
                            rest
                            (concat [:element
                                     (some-> message .getSource .getName)])))]
      (try
        (-> s parse clojure.walk/keywordize-keys
            (dissoc :pending-state :old-state))
        (catch Throwable e s)))))

(def str->map (fn [o] (-> (str o)
                          (str/split #";")
                          first
                          (str/split #", ")
                          (->> (mapv #(str/split % #"=")))
                          (update 0 (partial into [:type]))
                          (->> (into {}))
                          clojure.walk/keywordize-keys)))

(def log (comp println <-str :message))

(defn bus [element]
  (or
   (some-> element :reference .getBus)
   (some-> @element :root-bin .getBus)))

(def handler-dispatch (fn [sender k _] [(type sender) k]))

(defmulti handler handler-dispatch)

(defn on [o k f]
  (let [sender (cond-> o
                 (= clojure.lang.Atom (type o))
                 (some-> deref :root-bin .getBus))]
    (.connect sender (handler sender k f)))
  o)

(defmethod handler [org.freedesktop.gstreamer.Bus
                    :message]
  [_ _ f]
  (reify Bus$MESSAGE
    (busMessage [_ bus message]
      (f {:bus bus :message message}))))

(defmethod handler [org.freedesktop.gstreamer.Bus
                    :error]
  [_ _ f]
  (reify Bus$ERROR
    (errorMessage [_ _ code message]
      (f {:code code
          :message message}))))

(defmethod handler [org.freedesktop.gstreamer.Bus
                    :end-of-stream]
  [_ _ f]
  (reify org.freedesktop.gstreamer.Bus$EOS
    (endOfStream [_ source]
      (f {:source source}))))

(defmethod handler [org.freedesktop.gstreamer.elements.AppSink
                    :new-sample]
  [_ _ f]
  (reify org.freedesktop.gstreamer.elements.AppSink$NEW_SAMPLE
    (newSample [_ el]
      (f {:element el}))))

(comment
  (let [graph (test/tone)]
    (doto graph
      (on :message log)
      (graph/set-state! :playing)
      (graph/set-state! :paused)))
  nil)
