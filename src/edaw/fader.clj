(ns edaw.fader
  (:require [membrane.component :as mc]
            [membrane.ui :as ui]
            [edaw.ui]))

(mc/defui fader [{:keys [w h id touched? level axis]
                  :or {level 0.5 w 100 h 300 axis :y}}]
  (let [┫ #({:x [%1 %2] :y [%2 %1]} axis)
        ━ (comp first ┫) ┃ (comp second ┫)
        ->level (fn [[x y]] (━ (/ x w) (inc (/ y h -1))))]
    (ui/on
     :mouse-event (fn [pos _ down? _]
                    [[:set $touched? down?]
                     [::pressed {:id id :level (->level pos)}]])
     :mouse-move (fn [pos]
                   (when touched?
                     (let [l (->level pos)]
                       [[:set $level l]
                        [::moved {:id id :level l}]])))
     (let [foreground-dims (┫ (━ (* w level) (- h (* h level)))
                              (┃ w h))]
       [(edaw.ui/color (┃ :black :gray)
                       (ui/rectangle w h))
        (edaw.ui/color (━ :black :gray)
                       (apply ui/rectangle foreground-dims))]))))

#_(do (mc/defui fader-panner [{:keys [faders]}]
        (let [[a b] faders]
          (ui/vertical-layout
           [(fader a) (edaw.ui/color :white (ui/label (float (:level a))))]
           [(fader b) (edaw.ui/color :white (ui/label (float (:level b))))])))
      (membrane.java2d/run
       (mc/make-app #'fader-panner
                    {:faders [{:w 150 :h 300 :axis :y :channel-index 0 :level (rand)}
                              {:w 150 :h 50 :axis :x :channel-index 0 :level (rand)}]})
       {:window-start-width 150
        :window-start-height 420}))
