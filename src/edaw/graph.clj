(ns edaw.graph
  (:require [gstreamer.graph.low-level :as gll])
  (:import [org.freedesktop.gstreamer Format]
           [org.freedesktop.gstreamer.event SeekFlags]))

(def element! (partial reduce-kv gll/create-element!))
(def link! (partial reduce-kv gll/connect-elements!))
(def set-state! gll/set-state!)

(defn sync! [graph ks]
  (->> ks
       (map #(-> @graph % :reference .syncStateWithParent))
       dorun))

(defn seek! [graph position]
  (-> @graph
      :root-bin
      (.seekSimple Format/TIME
                   (java.util.EnumSet/of SeekFlags/FLUSH SeekFlags/KEY_UNIT)
                   (long position))))

(defn duration [graph] (-> @graph :root-bin (.queryDuration Format/TIME)))
