(ns edaw.mixer
  (:require [clojure.core.async :refer [go <! timeout]]
            [clojure.string :as str]
            [membrane.component :as mc]
            [membrane.ui :as ui]
            [membrane.java2d :as java2d]
            [edaw.test :as test]
            [edaw.element :as element]
            [edaw.util :as util]
            [edaw.ui]
            [edaw.track :as track]
            [edaw.fader :as fader :refer [fader]]
            [edaw.transport :as transport]
            [edaw.message :as message]
            [edaw.toggle :as toggle]
            [edaw.graph :as g]
            [gstreamer.graph.low-level :as gll]))

(defn ->id [n i] (keyword (str (name n) "-" i)))

(defn el-ref [graph id] ref (some-> graph deref (get id) :reference))

(mc/defeffect ::volume [{:keys [graph id level]}]
  (some-> graph (el-ref id) (.set "volume" level)))

(mc/defeffect ::pan [{:keys [graph id level]}]
  (some-> graph (el-ref id) (.set "panorama" (- level))))

(mc/defeffect ::mute [{:keys [graph id on?]}]
  (some-> graph deref :MIX :reference .getSinkPads (nth id nil) (.set "mute" (not on?))))

(mc/defeffect ::solo [{:keys [graph id on? channels $channels]}]
  (let [this? #(-> % :id #{id})]
    (dispatch! :update $channels
               (partial mapv #(if (this? %)
                                (assoc % :muted? false)
                                (assoc % :muted? (not on?) :solo? false))))
    (doseq [c channels]
      (dispatch! ::mute (into c {:graph graph
                                 :on? (or on? (this? c))})))))

(mc/defui channel [{:keys [w h id graph muted? solo?]}]
  (ui/vertical-layout
   (ui/on ::fader/moved #(do [[::volume (into % {:graph graph})]])
          (fader {:w w :h (* h 3/5) :id (->id :volume id)}))
   (ui/on ::fader/moved
          #(do [[::pan (-> %
                           (assoc :graph graph)
                           (update :level (comp - util/normalize)))]])
          (fader {:w w :h (/ h 5) :id (->id :audiopanorama id) :axis :x}))
   (ui/horizontal-layout
    (edaw.ui/after ::toggle/pressed #(do [[::mute (-> %
                                                      (into {:graph graph
                                                             :id id}))]])
                   (toggle/mute {:w (/ w 2) :h (/ h 5) :on? muted?}))
    (edaw.ui/after ::toggle/pressed (fn [_] [[::solo {:id id :on? solo?}]])
                   (toggle/solo {:w (/ w 2) :h (/ h 5) :on? solo?})))))

(mc/defui mixer [{:keys [w h channels graph playing?]}]
  (let [transport (edaw.ui/after ::toggle/pressed
                                 #(do [[::transport/play-pause
                                        (assoc % :graph graph)]])
                                 (toggle/play {:on? playing? :w 100 :h 100}))]
    (ui/on ::fader/moved
           (fn [id level] [[::volume graph id level]])
           (apply ui/horizontal-layout
                  (into [transport]
                        (for [c channels]
                          (ui/on ::solo
                                 (fn [m] [[::solo
                                           (merge m {:channels channels
                                                     :$channels $channels
                                                     :graph graph})]])
                                 (channel (assoc c :graph graph)))))))))

#_(let [graph (-> (test/deinterleaved-src!)
                  (g/element! {:audiomixer :MIX
                               :autoaudiosink :SINK})
                  (g/link! {:MIX :SINK}))]
    (-> @graph :DEINTERLEAVE
        (element/on :pad-added
                    (fn [{:keys [pad]}]
                      (let [id (->> pad
                                    .getName
                                    (re-matches #".*_([0-9]+)")
                                    second)]
                        (track/add-channel! graph id pad :MIX)))))
    (message/on graph :message message/log)
    (java2d/run
     (mc/make-app
      #'mixer
      {:channels (mapv #(do {:w 150 :h 300 :id % :muted? false})
                       (range 8))
       :graph graph})
     {:window-start-height 400}))
