(ns edaw.util)

(defn scale
  ([n to] (scale n 0 to))
  ([n from to] (+ from (* n (- to from))))
  ([n bottom top from to]
   (scale (/ (- n bottom)
             (- top bottom))
          from to)))

(defn normalize [n] (scale n -1 1))
