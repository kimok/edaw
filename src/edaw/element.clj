(ns edaw.element
  (:import [org.freedesktop.gstreamer
            Element$PAD_ADDED
            ElementFactory]))

(defmulti handle (fn [fn-name f] fn-name))

(defn on [element message-type f]
  (doto element
    (-> :reference
        (.connect (handle message-type f)))))

(defmethod handle :pad-added [_ f]
  (reify Element$PAD_ADDED
    (padAdded [_ el pad]
      (f {:element el :pad pad}))))

(defn make! [graph el-name el-type]
  (ElementFactory/make
   (name el-type)
   (name el-name)))
