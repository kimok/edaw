(defun edaw/play-file-at-point () (interactive)
       (require 'cider)
       (require 'magit)
       (let* ((dd default-directory)
              (magit-dd magit--default-directory))
         (cd "~/edaw")
         (if (not (cider-current-repl 'clj))
             (progn (cider-jack-in-clj '(:project-dir "~/edaw"))
                    (sit-for 5)))
         (cider-eval-file "~/edaw/src/edaw/launch.clj")
         (sit-for 0.5)
         (let* ((arg (prin1-to-string
           `(edaw.launch/player!
             ,(substring-no-properties
               (or
                (dired-get-filename nil t)
                (magit-file-at-point t)))))))
           (message arg)
           (cider-interactive-eval arg)
           (cd dd))))

(leader-key
  "a" 'edaw/play-file-at-point)
